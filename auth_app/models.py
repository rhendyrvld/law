from email.mime import application
from os import access
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    npm = models.CharField(max_length=10)

class ClientApp(models.Model):
    client_id = models.CharField(max_length=10, unique=True)
    client_secret = models.CharField(max_length=10, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.client_id
    
class AuthToken(models.Model):
    access_token = models.CharField(max_length=40, unique=True)
    refresh_token = models.CharField(max_length=40, unique=True)
    access_token_expiry = models.DateTimeField()
    refresh_token_expiry = models.DateTimeField()
    expired = models.BooleanField(default=False)
    client = models.OneToOneField(ClientApp, related_name="client_app", on_delete=models.CASCADE)

    def __str__(self):
        return self.access_token
    