import random
import string

chars = string.ascii_letters + string.digits

def generate_client_id_secret():
    return(''.join(random.choice(chars) for i in range (5)))

def generate_token():
    return(''.join(random.choice(chars) for i in range(40)))