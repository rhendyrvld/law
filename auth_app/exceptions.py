from email.policy import default
from rest_framework.exceptions import NotAuthenticated

class InvalidToken(NotAuthenticated):
    status_code = 401
    default_detail = 'Token Salah Bro'
    default_code = 'invalid_token'