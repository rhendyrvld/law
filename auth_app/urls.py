from django.urls import path
from auth_app.views import CreateAccessTokenView, GetResource

urlpatterns = [
    path('token/', CreateAccessTokenView.as_view(), name='token'),
    path('resource/', GetResource.as_view(), name='resource')
]