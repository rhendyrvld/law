from ast import For, Mult
from pydoc import cli
from time import time
from django.shortcuts import render
from django.utils import timezone
from django.contrib.auth import authenticate
from datetime import timedelta

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from auth_app.models import User, ClientApp, AuthToken
from auth_app.generators import generate_client_id_secret, generate_token
from auth_app.auth import CustomAuthentication


class CreateAccessTokenView(APIView):
    authentication_classes = []
    permission_classes = []
    parser_classes = [JSONParser, FormParser, MultiPartParser]

    def post(self, request, *args, **kwargs):
        try:
            username = request.data["username"]
            password = request.data["password"]
            client_id = request.data["client_id"]
            client_secret = request.data["client_secret"]
            user = authenticate(username=username,password=password)
            if user is not None:
                try:
                    client = ClientApp.objects.get(client_id=client_id, client_secret=client_secret)
                    try:
                        token = AuthToken.objects.get(client=client)
                        if (timezone.now() < token.access_token_expiry) and (timezone.now() < token.refresh_token_expiry):
                            data = {
                                "access_token" : token.access_token,
                                "expires_in" : 300,
                                "token_type" : "Bearer",
                                "scope" : None,
                                "refresh_token" : token.refresh_token
                            }
                        else:
                            token.access_token = generate_token()
                            token.access_token_expiry = timezone.now() + timedelta(minutes=5)
                            token.refresh_token = generate_token()
                            token.refresh_token_expiry = timezone.now() + timedelta(days=30)
                            token.save()
                            data = {
                                "access_token" : token.access_token,
                                "expires_in" : 300,
                                "token_type" : "Bearer",
                                "scope" : None,
                                "refresh_token" : token.refresh_token
                            }
                    except Exception:
                        newToken = AuthToken.objects.create(
                            access_token=generate_token(),
                            access_token_expiry=timezone.now() + timedelta(minutes=5),
                            refresh_token=generate_token(),
                            refresh_token_expiry=timezone.now() + timedelta(days=30),
                            client=client
                        )
                        data = {
                                "access_token" : newToken.access_token,
                                "expires_in" : 300,
                                "token_type" : "Bearer",
                                "scope" : None,
                                "refresh_token" : newToken.refresh_token
                        }
                        return Response(data=data, status=status.HTTP_200_OK)
                except Exception:
                    data = {
                        "error" : "invalid_request",
                        "Error_description" : "ada kesalahan!"
                    }
                    return Response(data=data, status=status.HTTP_401_UNAUTHORIZED)
            else:
                data = {
                        "error" : "invalid_request",
                        "Error_description" : "ada kesalahan!"
                    }
                return Response(data=data, status=status.HTTP_401_UNAUTHORIZED)
        except Exception:
            data = {
                    "error" : "invalid_request",
                    "Error_description" : "ada kesalahan!"
                }
            return Response(data=data, status=status.HTTP_401_UNAUTHORIZED)
        return Response(data=data, status=status.HTTP_200_OK)

class GetResource(APIView):
    authentication_classes = [CustomAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        user = request.user
        full_name = user.first_name + ' ' + user.last_name
        scheme, key = request.META['HTTP_AUTHORIZATION'].split(" ", 1)
        token = AuthToken.objects.get(access_token=key)
        data = {
            "access_token": token.access_token,
            "client_id" : token.client.client_id,
            "user_id": user.username,
            "full_name": full_name,
            "npm": user.npm,
            "expires": None,
            "refresh_token": token.refresh_token
        }
        return Response(data=data, status=status.HTTP_200_OK)