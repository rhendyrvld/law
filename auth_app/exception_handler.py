from rest_framework.views import exception_handler
from rest_framework.response import Response

from auth_app.exceptions import InvalidToken

def custom_exception_handler(exc, context):
    if isinstance(exc, InvalidToken):
        response = exception_handler(exc, context)
        if response is not None:
            del response.data['detail']
            response.data['error'] = InvalidToken.default_code
            response.data['error_description'] = InvalidToken.default_detail
    return response