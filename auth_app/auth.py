from xml.dom import NotFoundErr
from django.utils import timezone
from rest_framework.authentication import BaseAuthentication
from auth_app.exceptions import InvalidToken
from auth_app.models import AuthToken


class CustomAuthentication(BaseAuthentication):
    def authenticate(self, request, **kwargs):
        try:
            auth_header_value = request.META.get("HTTP_AUTHORIZATION", "")
            if auth_header_value:
                auth_scheme, key = request.META["HTTP_AUTHORIZATION"].split(" ", 1)
                if not key:
                    raise InvalidToken()
                if not auth_scheme.lower() == "bearer":
                    raise InvalidToken()
                token = CustomAuthentication.verify_token(request,key)
                return (token, None)
            else:
                raise InvalidToken()
        except InvalidToken as _e:
            raise InvalidToken()

    def verify_token(request, key):
        if AuthToken.objects.filter(access_token=key).exists():
            token = AuthToken.objects.get(access_token=key)
            user = token.client.user
            if token.access_token_expiry < timezone.now():
                token.expired = True
                token.save()
                raise InvalidToken()
            return user
        else:
            raise InvalidToken()